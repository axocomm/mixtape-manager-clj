(ns playlist-manager.errors)

(def
  ^{:doc "Error codes and corresponding message formats."}
  error-codes
  {:no-such-user       "User %s does not exist"
   :no-such-song       "Song %s does not exist"
   :no-such-songs      "Songs (%s) do not exist"
   :no-such-playlist   "Playlist %s does not exist"
   :no-songs           "New playlist must contain at least one song"
   :unsupported-action "Action '%s' is not supported"})

(defn except!
  "Throw an exception.

  This function looks up the appropriate message or format from
  `error-codes` and accepts a variable number of arguments to format
  into it. If no such error code exists, an exception with a default
  message will be thrown."
  [code & args]
  (let [code-msg (if-let [fmt (get error-codes code)]
                   (apply format (concat [fmt] args))
                   (format "%s: %s" (name code) args))]
    (throw (Exception. code-msg))))
