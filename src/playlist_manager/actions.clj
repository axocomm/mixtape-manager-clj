(ns playlist-manager.actions
  (:require
   [clojure.string :as string]
   [playlist-manager.errors :refer [except!]]
   [playlist-manager.helpers :refer [uniq]]
   [playlist-manager.mixtape-helpers :as h]))

(defn add-playlist
  "Add a new playlist to the given `mixtape`.

  If both the `user-id` and all `song-ids` exist within the
  `mixtape` (and any `song-ids` are provided in the first place), this
  returns a new `mixtape` containing a new playlist for the `user-id`
  and `song-ids` provided. Otherwise, an appropriate exception is
  thrown."
  [mixtape {:keys [user-id song-ids]}]
  (cond
    (empty? song-ids)
    (except! :no-songs)

    (not (h/user-exists? mixtape user-id))
    (except! :no-such-user user-id)

    ;; FIXME: This can probably be handled more succinctly.
    (not (every? (partial h/song-exists? mixtape) song-ids))
    (let [missing-songs (remove (partial h/song-exists? mixtape) song-ids)]
      (except! :no-such-songs (string/join ", " missing-songs)))

    :else
    (update mixtape :playlists conj (h/make-playlist
                                     mixtape
                                     user-id
                                     song-ids))))

(defn add-song
  "Add a new song to the given playlist in `mixtape`.

  If the provided playlist and song exist in `mixtape`, this returns a
  new `mixtape` with that song added to the playlist. Otherwise, an
  appropriate exception is thrown.

  TODO: Maybe consider schema? e.g. this 'works' if no song-id is passed..."
  [mixtape {:keys [playlist-id song-id]}]
  (cond
    (not (h/playlist-exists? mixtape playlist-id))
    (except! :no-such-playlist playlist-id)

    (not (h/song-exists? mixtape song-id))
    (except! :no-such-song song-id)

    :else
    (let [new-playlists (map (fn [playlist]
                               (if (= (:id playlist) playlist-id)
                                 (assoc
                                  playlist
                                  :song_ids
                                  (-> (:song_ids playlist)
                                      (conj song-id)
                                      uniq))
                                 playlist))
                             (:playlists mixtape))]
      (assoc mixtape :playlists new-playlists))))

(defn remove-playlist
  "Remove a playlist by ID.

  If the playlist referenced by `playlist-id` exists in `mixtape`, a
  new `mixtape` is returned with that playlist removed. Otherwise, an
  exception is thrown."
  [mixtape {:keys [playlist-id]}]
  (if-not (h/playlist-exists? mixtape playlist-id)
    (except! :no-such-playlist playlist-id)
    (let [new-playlists (remove #(= (:id %) playlist-id)
                                (:playlists mixtape))]
      (assoc mixtape :playlists new-playlists))))
