(ns playlist-manager.mixtape-helpers
  (:require
   [playlist-manager.helpers :refer [detect parse-int]]))

(defn find-by-id
  "Find something in the mixtape by ID.

  If the given collection or element does not exist, `nil` is
  returned."
  [mixtape coll-key id]
  (when-let [coll (get mixtape coll-key)]
    (detect #(= (:id %) id) coll)))

(defn find-user
  "Fetch a user by ID."
  [mixtape user-id]
  (find-by-id mixtape :users user-id))

(defn find-playlist
  "Fetch a playlist by ID."
  [mixtape playlist-id]
  (find-by-id mixtape :playlists playlist-id))

(defn find-song
  "Fetch a song by ID."
  [mixtape song-id]
  (find-by-id mixtape :songs song-id))

(defn next-id
  "Try to determine the next available ID of a given collection inside
  `mixtape` by simply incrementing the max `id`.

  This first fetches the given `coll-key` from `mixtape` (`:songs`,
  `:playlists`, etc.). It then collects and parses to Integer all
  `:id`s and determines the maximum value among them. Finally, it
  increments that value and turns that result back into a String (as
  these entries are keyed by).

  If the collection specified by `coll-key` doesn't exist, this
  returns a `default` (if provided) or `nil` instead."
  ([mixtape coll-key]
   (next-id mixtape coll-key nil))
  ([mixtape coll-key default]
   (let [coll (get mixtape coll-key)]
     (if (or (empty? coll) (nil? coll))
       default
       (->> coll
            (map (comp parse-int :id))
            (apply max)
            inc
            str)))))

(defn make-playlist
  "Create a new `playlists` entry given a `user-id` and `song-ids`."
  [mixtape user-id song-ids]
  {:id       (next-id mixtape :playlists)
   :user_id  user-id
   :song_ids song-ids})

(defn user-exists?
  "Check whether the given user with ID `user-id` exists in `mixtape`."
  [mixtape user-id]
  (not (nil? (find-user mixtape user-id))))

(defn playlist-exists?
  "Check whether the given playlist with ID `playlist-id` exists in
  `mixtape`."
  [mixtape playlist-id]
  (not (nil? (find-playlist mixtape playlist-id))))

(defn song-exists?
  "Check whether the given song with ID `song-id` exists in
  `mixtape`."
  [mixtape song-id]
  (not (nil? (find-song mixtape song-id))))
