(ns playlist-manager.helpers)

(defn parse-int
  "Try to parse a string to an Integer, returning nil on failure.

  TODO: Maybe this should throw instead, as much of the change
  application process is doing."
  [s]
  (try
    (Integer/parseInt s)
    (catch NumberFormatException _
      nil)))

(def
  ^{:doc "Return the first collection element, if it exists, satisfying the given predicate."}
  detect
  (comp first filter))

(def
  ^{:doc "Generate a vector of unique elements of the given collection."}
  uniq
  (comp vec set))
