(ns playlist-manager.core
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]
   [cheshire.core :as json]
   [playlist-manager.changes :refer [apply-changes]]
   [playlist-manager.helpers :refer [parse-int detect]]
   [playlist-manager.mixtape-helpers :as h])
  (:gen-class))

(def
  ^{:doc "The default mixtape filename in the resources directory."}
  default-mixtape-file
  "resources/mixtape.json")

(def
  ^{:doc "The default change filename in the resources directory."}
  default-change-file
  "resources/changes.json")

;; TODO: These should be accepting full file paths.
(defn load-mixtape
  "Load a mixtape from the given JSON file.

  If not passed an argument, this will default to
  `default-mixtape-file`.

  TODO: In case the mixtape input is incomplete for whatever reason,
  this should merge this with a base mixtape payload to ensure that
  all entity keys are present and populated, even with an empty list."
  ([]
   (load-mixtape default-mixtape-file))
  ([filename]
   (-> (io/file filename)
       slurp
       (json/parse-string keyword))))

(defn load-changes
  "Load mixtape changes from the given JSON file.

  If not passed an argument, this will default to
  `default-change-file`."
  ([]
   (load-changes default-change-file))
  ([filename]
   (-> (io/file filename)
       slurp
       (json/parse-string keyword))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (if-let [[mixtape-file changes-file] args]
    (try
      (let [mixtape     (load-mixtape mixtape-file)
            changes     (load-changes changes-file)
            new-mixtape (apply-changes mixtape changes)]
        (println (json/generate-string new-mixtape)))
      (catch Exception e
        (printf "!!! Got exception: %s\n" (.getMessage e))))
    (println "Missing required mixtape and change filenames")))
