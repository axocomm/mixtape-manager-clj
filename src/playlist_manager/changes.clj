(ns playlist-manager.changes
  (:require
   [playlist-manager.actions :refer [add-playlist add-song remove-playlist]]
   [playlist-manager.errors :refer [except!]]))

(def
  ^{:doc "Supported mixtape changes."}
  change-types
  {"ADD_PLAYLIST"    :add-playlist
   "ADD_SONG"        :add-song
   "REMOVE_PLAYLIST" :remove-playlist})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Multimethod for change application.                                  ;;
;;                                                                      ;;
;; `apply-change` will first determine, based on the `action` specified ;;
;; in the given change entry, which of the action-specific functions to ;;
;; use. If no change type can be resolved given the `action` field, an  ;;
;; exception is thrown instead indicating an unsupported change.        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti apply-change
  (fn [_ {:keys [action]}]
    (get change-types action)))

(defmethod apply-change :add-playlist
  [mixtape {:keys [payload]}]
  (add-playlist mixtape payload))

(defmethod apply-change :add-song
  [mixtape {:keys [payload]}]
  (add-song mixtape payload))

(defmethod apply-change :remove-playlist
  [mixtape {:keys [payload]}]
  (remove-playlist mixtape payload))

(defmethod apply-change nil
  [_ {:keys [action]}]
  (except! :invalid-action action))

(defn apply-changes
  "Apply all changes specified to the given `mixtape`.

  This simply reduces over `changes` and repeatedly calls
  `apply-change` for each one, applying said changes to `mixtape`. If
  any issues occur, whether they be missing songs, nonexistend users,
  or an unsupported change type, an exception as defined in
  `playlist-manager.errors.error-codes` is thrown."
  [mixtape changes]
  (reduce apply-change mixtape changes))
