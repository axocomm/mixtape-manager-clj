(ns playlist-manager.mixtape-helpers-test
  (:require
   [clojure.test :refer :all]
   [playlist-manager.mixtape-helpers :as h]
   [playlist-manager.test-common :refer [testing-mixtape]]))

(deftest user-location-test
  (testing "find-user should be able to fetch a user."
    (let [expected-user {:id "1", :name "Foo Bar"}]
      (is (= (h/find-user testing-mixtape "1")
             expected-user))))

  (testing "user-exists? should return true or false depending on existence of the given user."
    (let [good-user-id "1"
          bad-user-id  "1234"]
      (is (and (true? (h/user-exists? testing-mixtape good-user-id))
               (false? (h/user-exists? testing-mixtape bad-user-id)))))))

(deftest playlist-location-test
  (testing "find-playlist should be able to fetch a playlist."
    (let [expected-playlist {:id "1", :user_id "1", :song_ids ["1", "2"]}]
      (is (= (h/find-playlist testing-mixtape "1")
             expected-playlist))))

  (testing "playlist-exists? should return true or false depending on existence of the given playlist."
    (let [good-playlist-id "1"
          bad-playlist-id  "1234"]
      (is (and (true? (h/playlist-exists? testing-mixtape good-playlist-id))
               (false? (h/playlist-exists? testing-mixtape bad-playlist-id)))))))

(deftest song-location-test
  (testing "find-song should be able to fetch a song."
    (let [expected-song {:id "1", :artist "Baz Quux", :title "The Song Name"}]
      (is (= (h/find-song testing-mixtape "1")
             expected-song))))

  (testing "song-exists? should return true or false depending on existence of the given song."
    (let [good-song-id "1"
          bad-song-id  "1234"]
      (is (and (true? (h/song-exists? testing-mixtape good-song-id))
               (false? (h/song-exists? testing-mixtape bad-song-id)))))))

(deftest next-id-test
  (testing "next-id should be able to produce the next sequential ID of the given collection."
    (let [expected-id "3"]
      (is (= (h/next-id testing-mixtape :playlists) expected-id))))

  (testing "next-id should return `nil` if the given collection does not exist in `mixtape` and a `default` is not provided."
    (is (nil? (h/next-id testing-mixtape :nonsense))))

  (testing "next-id should return a default value if the given collection does not exist in `mixtape` and a `default` is provided."
    (is (= (h/next-id testing-mixtape :nonsense -1) -1)))

  (testing "next-id should return `nil` if the given collection exists but is empty."
    (is (nil? (h/next-id (assoc testing-mixtape :users []) :users)))))

(deftest make-playlist-test
  (testing "make-playlist should be able to produce a valid :playlists entry for the mixtape, including leveraging next-id."
    (let [expected-rec {:id       "3"
                        :user_id  "1"
                        :song_ids ["1", "2", "3"]}]
      (is (= (h/make-playlist
              testing-mixtape
              "1"
              ["1", "2", "3"])
             expected-rec)))))
