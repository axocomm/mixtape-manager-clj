(ns playlist-manager.helpers-test
  (:require
   [clojure.test :refer :all]
   [playlist-manager.helpers :refer [parse-int detect uniq]]))

(deftest helpers-test
  (testing "parse-int should be able to parse an Integer string and return `nil` otherwise."
    (is (and (nil? (parse-int "foobar"))
             (= 270 (parse-int "270")))))

  (testing "detect should return the first element in a list satisfying the predicate and nil otherwise."
    (let [test-coll     (range 2 10)
          expected-even 2
          even-match    (detect even? test-coll)
          expected-odd  3
          odd-match     (detect odd? test-coll)
          no-match      (detect #(> % 10) test-coll)]
      (is (and
           (= expected-even even-match)
           (= expected-odd odd-match)
           (nil? no-match)))))

  (testing "uniq should return a vector of unique values in a collection."
    (let [test-coll [1 2 2 3 3 3 4 4 4 4]
          uniq-coll [1 2 3 4]]
      (is (= (sort uniq-coll) (sort (uniq test-coll))))))

  (testing "uniq should not complain about empty or nil collections."
    (is (and (= (uniq []) [])
             (= (uniq nil) [])))))
