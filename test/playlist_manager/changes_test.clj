(ns playlist-manager.changes-test
  (:require
   [clojure.test :refer :all]
   [playlist-manager.changes :refer [apply-change]]
   [playlist-manager.test-common :refer [testing-mixtape]]))

(deftest change-types-test
  (testing "apply-change should throw if the given change type is invalid."
    (let [bad-change {:action  "NOT_VALID"
                      :payload {}}]
      (is (thrown? Exception
                   (apply-change testing-mixtape bad-change))))))
