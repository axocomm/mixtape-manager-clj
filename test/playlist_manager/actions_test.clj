(ns playlist-manager.actions-test
  (:require
   [clojure.test :refer :all]
   [playlist-manager.actions :as a]
   [playlist-manager.helpers :refer [detect]]
   [playlist-manager.mixtape-helpers :as h]
   [playlist-manager.test-common :refer [testing-mixtape]]))

(deftest add-playlist-test
  (let [user-id "1"]
    (testing "add-playlist should be able to add a new playlist given a user ID and song IDs."
      (let [song-ids        ["3"]
            new-mixtape     (a/add-playlist testing-mixtape {:user-id  user-id
                                                             :song-ids song-ids})
            new-playlist-id "3"]
        (is (and (h/playlist-exists? new-mixtape new-playlist-id)
                 (= (:song_ids (h/find-playlist new-mixtape new-playlist-id))
                    song-ids)))))

    (testing "add-playlist should throw if a song does not exist."
      (let [bad-song-ids ["123"]]
        (is (thrown? Exception
                     (a/add-playlist testing-mixtape {:user-id  user-id
                                                      :song-ids bad-song-ids})))))

    (testing "add-playlist should throw if the user does not exist."
      (let [bad-user-id "1234"]
        (is (thrown? Exception
                     (a/add-playlist testing-mixtape {:user-id  bad-user-id
                                                      :song-ids ["1"]})))))

    (testing "add-playlist should throw if no songs are provided."
      (is (thrown? Exception
                   (a/add-playlist testing-mixtape {:user-id  user-id
                                                    :song-ids []}))))))

(deftest add-song-test
  (let [playlist-id "1"]
    (testing "add-song should be able to add an existing song to an existing playlist."
      (let [new-song-id       "3"
            existing-song-ids (:song_ids (h/find-playlist
                                          testing-mixtape
                                          playlist-id))
            new-mixtape       (a/add-song testing-mixtape
                                          {:playlist-id playlist-id
                                           :song-id     new-song-id})
            new-song-ids      (:song_ids (h/find-playlist
                                          new-mixtape
                                          playlist-id))]
        (is (= (sort new-song-ids)
               (sort (conj existing-song-ids new-song-id))))))

    (testing "add-song should throw if the given playlist does not exist."
      (let [bad-playlist-id "1234"]
        (is (thrown? Exception
                     (a/add-song testing-mixtape
                                 {:playlist-id bad-playlist-id
                                  :song-id     "1"})))))

    (testing "add-song should throw if the given song does not exist."
      (let [bad-song-id "1234"]
        (is (thrown? Exception
                     (a/add-song testing-mixtape
                                 {:playlist-id playlist-id
                                  :song-id     bad-song-id})))))

    (testing "add-song shouldn't add a duplicate of a song if it already exists."
      (let [existing-song-id "1"
            new-mixtape      (a/add-song
                              testing-mixtape
                              {:playlist-id playlist-id
                               :song-id     existing-song-id})]
        (is (= (count (filter
                       #(= (:id %) existing-song-id)
                       (:songs new-mixtape)))
               1))))))

(deftest remove-playlist-test
  (testing "remove-playlist should be able to remove an existing playlist."
    (let [playlist-id "1"
          new-mixtape (a/remove-playlist testing-mixtape
                                         {:playlist-id playlist-id})]
      (is (nil? (detect #(= (:id %) playlist-id) (:playlists new-mixtape))))))

  (testing "remove-playlist should throw if the given playlist does not exist."
    (let [bad-playlist-id "3"]
      (is (thrown? Exception
                   (a/remove-playlist testing-mixtape
                                      {:playlist-id bad-playlist-id}))))))
