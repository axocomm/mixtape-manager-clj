(ns playlist-manager.test-common)

(def
  ^{:doc "A test mixtape."}
  testing-mixtape
  {:users     [{:id   "1"
                :name "Foo Bar"}]
   :playlists [{:id       "1"
                :user_id  "1"
                :song_ids ["1", "2"]}
               {:id       "2"
                :user_id  "1"
                :song_ids ["2", "3"]}]
   :songs     [{:id     "1"
                :artist "Baz Quux"
                :title  "The Song Name"}
               {:id     "2"
                :artist "Spam Eggs"
                :title  "Another Song Name"}
               {:id     "3"
                :artist "Beep Boop"
                :title  "One More Song Name"}]})
