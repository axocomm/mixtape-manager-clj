# playlist-manager

A simple mixtape manager that accepts a JSON payload representing a
mixtape and allows for application of several changes.

## Requirements

- Java
- Leiningen (if not running via JAR as well as for testing/building)

## Usage

### Building

A JAR file is provided with this repository, but if desired, to build
a new JAR, simply run `lein uberjar`. This should produce a JAR file
in the `target/uberjar` directory.

### Running

To run with Leiningen, you may do so invoke as follows:

    % lein run -- <mixtape-json> <changes-json>

Alternatively, a JAR file is provided which can be run similarly,
e.g.:

    % java -jar target/uberjar/playlist-manager-0.1.0-SNAPSHOT-standalone.jar \
      resources/mixtape.json \
      resources/changes.json

Example `mixtape.json` and `changes.json` are provided with this
repository in the `resources` directory.

## Testing

To test, you will need Leiningen installed. Simply run `lein test` and
you should receive output like the following:

    lein test playlist-manager.actions-test

    lein test playlist-manager.changes-test

    lein test playlist-manager.core-test

    lein test playlist-manager.helpers-test

    lein test playlist-manager.mixtape-helpers-test

    Ran 11 tests containing 27 assertions.
    0 failures, 0 errors.

Test cases may be found in the `test/playlist_manager` directory:

    test
    └── playlist_manager
        ├── actions_test.clj
        ├── changes_test.clj
        ├── core_test.clj
        ├── helpers_test.clj
        ├── mixtape_helpers_test.clj
        └── test_common.clj

and contain cases that evaluate core functionality of this application
using a fixture mixtape defined in `test-common/testing-mixtape`.

## How it Works

This application simply reads in JSON files containing a mixtape
(users, playlists, and songs) as well as a list of changes to apply
and reduces said set of changes over that mixtape. Each iteration of
the accumulating function receives a new copy of `mixtape` with the
previous changes applied. If at any point an exceptional condition is
experience, for simplicity's sake an error is thrown and execution is
halted.

### Architecture

    src
    └── playlist_manager
        ├── actions.clj
        ├── changes.clj
        ├── core.clj
        ├── errors.clj
        ├── helpers.clj
        └── mixtape_helpers.clj

#### `core.clj`

This is just the entry point of the application and handles reading in
the mixtape and change JSON files.

#### `changes.clj`

This file contains the `apply-changes` function as well as the
`apply-change` multimethod, which is reduced over the supplied
`changes` to `mixtape`.

The `apply-change` multimethod accepts a `change`, which itself
contains an `action` and a `payload`. Depending on the value of
`action`, the appropriate dispatch is chosen (with an exception being
thrown if the action is not supported) and applied to the `mixtape`.

#### `actions.clj`

This file contains implementations of the actions dictated by
`playlist-manager.changes/change-types`. Each of these functions
accepts the current `mixtape` as well as the `payload` of each
respective change as defined in the changes JSON.

These currently include the following:

- `add-playlist`

##### Example payload

``` json
{
  "action": "ADD_PLAYLIST",
  "payload": {
    "user-id": "2",
    "song-ids": [
      "7",
      "12"
    ]
  }
}
```

This function adds a new playlist to `mixtape`. This first checks the
following:

- whether song IDs are provided
- whether the given user exists
- whether each provided song ID exists in `mixtape.songs`

Should any of these checks fail, an exception is thrown. Otherwise,
`mixtape.playlists` is augmented with a new entry created via
`playlist-manager.helpers/make-playlist` (which just accepts the
mixtape (for generating a next ID), user ID, and song IDs and
generates a new map in the format of existing playlist entries
accordingly).

- `add-song`

##### Example payload

``` json
{
  "action": "ADD_SONG",
  "payload": {
    "playlist-id": "2",
    "song-id": "12"
  }
}
```

This function adds an existing song to an existing playlist, and first
checks the following:

- whether the given playlist exists in `mixtape.playlists`
- whether the given song exists in `mixtape.songs`

Should either of these conditions fail, an appropriate exception is
raised via `helpers/except!` (described below). Otherwise,
`mixtape.playlists` is mapped over, with the matching playlist (by ID)
simply having its `:song_ids` list augmented with the new song ID.

- `remove-playlist`

##### Example payload

``` json
{
  "action": "REMOVE_PLAYLIST",
  "payload": {
    "playlist-id": "1"
  }
}
```

This function removes a playlist by ID. If the playlist does not exist
in `mixtape.playlists`, an exception is thrown. Otherwise, a new
mixtape is returned with the given playlist omitted.

#### `errors.clj`

This namespace just contains a few known exception types (and message
formats) as well as an `except!` helper function that accepts the
error code and format arguments. A message is generated (if possible,
otherwise a default is rendered) and an `Exception` is thrown.

#### `helpers.clj`

This namespace contains a few helper functions:

- `parse-int` for safer conversion from `String` to `Integer`
  (returning `nil` instead of throwing a `NumberFormatException` on
  failure).

- `detect` to immediately return the first predicate-matching element
  of a collection or `nil` otherwise

- `uniq` to easily return a new vector of unique elements of a
  given collection

#### `mixtape_helpers.clj`

This namespace contains several mixtape-related helper functions:

- `find-by-id` to quickly find a mixtape element (song, playlist,
  user) by its ID (and wrapped by `find-user`, `find-playlist`, and
  `find-song`)

- `next-id` to generate the next sequential ID of a given mixtape
  collection, or an optional `default` otherwise (itself `nil` if not
  passed)

- `make-playlist` to generate a mixtape-compatible `playlists` entry
  containing newly-generated ID, user ID, and song IDs

- `user-exists?`, `playlist-exists?`, and `song-exists?` for quickly
  checking whether these elements exist in the given `mixtape` by ID
